# Validata-Badge

## Configuration

La configuration du badge Validata est contenue dans le fichier [badge_conf.toml](badge_conf.toml).

Elle indique le seuil (`acceptability-threshold`) au delà duquel le ratio
d'erreur de contenu passe de l'orange (acceptable ?) au rouge.
Chaque type d'erreur intervenant dans le calcul du taux d'erreur peut être
pondéré individuellement (`errors-weight`).

## Badge

La construction d'un badge nécessite de croiser les résultats de validation
d'une ressource et les informations de configuration du badge.

Un badge est constitué des informations suivantes :

- structure : les valeurs possibles sont `OK`, `WARN` et `KO`
  - `OK`: pas d'erreur de structure
  - `WARN`: erreur(s) de structure récupérable(s)
  - `KO`: erreur(s) de structure non récupérable(s)
- body: Les valeurs possibles sont `OK`, `WARN` et `KO`
  - `OK`: pas d'erreur de contenu
  - `WARN`: pourcentage d'erreur (`R`) inférieur au seuil d'acceptabilité (`Th`)
  - `KO`: pourcentage d'erreur (`R`) supérieur ou égal au seuil d'acceptabilité (`Th`)
- ratio (`R`) : pourcentage d'erreur (valeurs comprises entre 0 et 1)

Notes :

- Si la propriété `structure` vaut `KO`, les informations de `body` et `ratio` ne sont pas données.
- `R` = taux d'erreur (Error Ratio) calculé comme le nombre de cellules en erreur sur le nombre de cellules total (valeur comprise entre 0 et 1) pondéré par le poids sur le type d'erreur. Le poids associé à chaque type d'erreur est spécifié dans la configuration
- `Th` = seuil d'acceptabilité (Acceptability Threshold) à partir duquel le témoin passe de l'orange au rouge (valeur comprise entre 0 et 1). La valeur du seuil est spécifié dans la configuration.
