# Validation du fichier de configuration

Un script Python pour valider le fichier de configuration du badge Validata.

## Pré-requis

Pour installer les dépendances :

```bash
pip install -r requirements.txt
```

## Utilisation

```bash
python3 validate_conf.py ../badge_conf.md
```

En cas d'erreur, celle-ci est affichée. Le code de retour du script vaut `0` si aucune erreur n'est détectée, et `1` sinon.