#!/usr/bin/env python3

import argparse
import json
import sys
from pathlib import Path

import toml
from jsonschema import Draft7Validator
from jsonschema.exceptions import best_match


def main():
    """Validate configuration file"""
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('conf_file', type=Path, help='path of badge config file')
    args = parser.parse_args()

    conf_file = args.conf_file
    if not conf_file.exists():
        parser.error("Configuration file {!r} not found".format(str(conf_file)))

    try:
        conf = toml.load(str(conf_file))
    except toml.decoder.TomlDecodeError as exc:
        print(exc)
        return 1

    with (Path(__file__).parent / "schema.json").open() as fp:
        schema = json.load(fp)

    error = best_match(Draft7Validator(schema).iter_errors(conf))
    if error is not None:
        print(error)
        return 1

    return 0


if __name__ == '__main__':
    sys.exit(main())
