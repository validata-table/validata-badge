# Génération de la documentation markdown

Un script Python pour générer la documentation relative à la configuration
du badge Validata.

## Pré-requis

Le script nécessite la bibliothèque `toml` et `Jinja2`. Pour les installer :

```bash
pip install -r requirements.txt
```

## Personnalisation

La structure de la page de documentation est stockée dans le fichier templace doc_md.jinja2

## Génération de la documentation

```bash
python3 gen_doc.py ../badge_conf.md
```
