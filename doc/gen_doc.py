#!/usr/bin/env python3
"""
Generates markdown live documentation on badge configuration


"""
import argparse
import logging
import sys
from pathlib import Path

import toml
from jinja2 import Template

script_dir = Path(__file__).parent


def main():
    """Generates markdown doc from badge configuration file"""
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('conf_file', type=Path, help='path of badge config file')
    parser.add_argument('--log', default='WARNING', help='level of logging messages')
    parser.add_argument('--template', type=Path, default=script_dir / 'doc_md.jinja2', help='doc template file')
    parser.add_argument('--output', type=Path, default=None, help='output file path')
    args = parser.parse_args()

    numeric_level = getattr(logging, args.log.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: {}'.format(args.log))
    logging.basicConfig(
        format="%(levelname)s:%(name)s:%(asctime)s:%(message)s",
        level=numeric_level,
        stream=sys.stdout,  # Use stderr if script outputs data to stdout.
    )

    conf_file = args.conf_file
    if not conf_file.exists():
        parser.error("Configuration file {!r} not found".format(str(conf_file)))
    template_file = args.template
    if not template_file.exists():
        parser.error("Template file {!r} not found".format(str(template_file)))

    config = toml.load(str(conf_file))

    template = Template(template_file.open('rt', encoding='utf-8').read())
    result = template.render(threshold=config['body']['acceptability-threshold'],
                             weight_dict=config['body']['errors-weight'])

    fd = sys.stdout if args.output is None else args.output.open('wt', encoding='utf-8')
    print(result, file=fd)


if __name__ == '__main__':
    sys.exit(main())
