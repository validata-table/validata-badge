# Validata-badge Ansible playbooks

This directory contains Ansible playbooks useful to automate system administration tasks, required to execute GitLab CI/CD pipelines properly.

The server is supposed using Debian Stretch (the stable version as I write this).

## Install server

The [top-level playbook file](./install.yml) defines the steps to execute on the server. Run it with:

```sh
export GITLAB_API_PRIVATE_TOKEN="..." # secret, to be defined in your GitLab settings (https://git.opendatafrance.net/profile/personal_access_tokens)
export SCW_TOKEN="..." # secret, see Jailbreak passwords file. Comes from Scaleway Console (https://console.scaleway.com/account/credentials "Tokens" section; copy "Secret key")

cd ansible

# Install roles (to do only once)
ansible-galaxy install -r requirements.yml

ansible-playbook --inventory inventory --user root --limit validata install.yml
```
